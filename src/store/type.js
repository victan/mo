const type = {
  TokenChange: 'token/change',
  TokenPendingChange: 'token/pending/change',
  LangChange: 'language/change',
  MemberProfileChange: 'member/profile/change'
}

export default type
