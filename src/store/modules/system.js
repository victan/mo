// import type from './type'
// import axios from 'axios'
import moment from 'moment'

const state = {
  time: Date.now(),
  now: moment().format('YYYY-MM-DD HH:mm')
}

setTimeout(() => {
  state.time += 5000
  state.now = moment(state.time).format('YYYY-MM-DD HH:mm')
}, 5000)

const getters = {
  now: state => state.now
}

const mutations = {
}

var actions = {
}

export default ({
  state,
  getters,
  actions,
  mutations
})
