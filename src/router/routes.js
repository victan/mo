function _ (name) {
  return () => import(`@/view/${name}.vue`)
}

const routes = [
  {
    path: '/',
    name: 'home',
    component: _('Dashboard/Layout'),
    meta: {private: false, auth: ''},
    redirect: 'dashboard',
    children: [
      {
        path: '/dashboard',
        name: 'dashboard',
        component: _('Dashboard/Index'),
        meta: {private: true, auth: ''}
      }
    ]
  },
  {
    path: '/authentication',
    name: 'authentication',
    component: _('Authentication/Layout'),
    meta: {private: false, auth: ''},
    redirect: 'login',
    children: [
      {
        path: '/login',
        name: 'login',
        component: _('Authentication/Login'),
        meta: {private: false, auth: ''}
      }
    ]
  },
  {
    path: '/loading',
    name: 'loading',
    component: _('Application/Loading'),
    meta: {private: false, auth: ''}
  },
  {
    path: '/404',
    name: '404',
    component: _('Application/404'),
    meta: {private: false, auth: ''}
  },
  {
    path: '*',
    name: 'lost',
    meta: {private: false, auth: ''},
    redirect: '404'
  }
]

export default routes
