import { message } from 'ant-design-vue'

function toast (txt) {
  console.log('toast', txt)
  if (typeof txt === 'object') return
  message.error(txt)
}

export default {
  warn (trace, vm) {
    if (!trace) return
    try { console.log(trace.constructor.name, trace, vm) } catch (e) {}

    if (typeof trace === 'string') {
      return toast(trace)
    }

    // 当后台程序直接爆掉返回html页面时
    if (typeof trace.data === 'string') {
      return toast(trace.statusText)
    }

    if (trace.errors && Array.isArray(trace.errors)) {
      let messages = trace.errors.map(v => v.field ? `${v.field} ${v.message}` : v.message || v)
      return toast(messages.join('、'))
    }

    if (trace.message) {
      return toast(trace.message)
    }

    if (typeof trace === 'object') {
      return toast(trace.errors || trace.toString())
    }
  }
}
