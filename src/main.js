import Vue from 'vue'
import App from './App.vue'
import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css'
import config from '@/config/'
import i18n from './i18n'
import router from './router'
import store from './store'
import {mapGetters} from 'vuex'
import './asset/style/app.css'
import './asset/style/theme.css'
import './util/axios'
import toast from './util/toast'

Vue.use(Antd)

Vue.config.productionTip = false

Vue.mixin({
  computed: {
    ...mapGetters({
      _TOKEN_: 'token',
      _NOW_: 'now'
    }),
    logged () {
      return !!store.state.authorization.token
    },
    $host () {
      return config.host
    },
    lang () {
      return i18n.messages[i18n.locale]
    },
    _dir_ () {
      return process.env.PORTABLE_EXECUTABLE_DIR || process.env.PWD
    },
    _locale_ () {
      return i18n.locale
    }
  }
})

Vue.prototype.warn = v => toast.warn(v)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App),
  store,
  router,
  i18n
});
