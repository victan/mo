const path = require('path');
const webpack = require('webpack');

const host = process.env.SERVER || process.env.npm_config_env || process.env.NODE_ENV || 'local'
const map = {
  production: 'prod',
  development: 'local'
}

function resolveSrc(_path) {
  return path.join(__dirname, _path);
}
// vue.config.js
module.exports = {
  lintOnSave: true,
  chainWebpack: (config) => {
    config.resolve.alias
      .delete('@')
      .set('@/config', `${resolveSrc('src')}/config/${map[host] || host}.js`)
      .set('@', resolveSrc('src'))
  },
  configureWebpack: {
    externals: {
      'vue': 'Vue',
      'vue-router': 'VueRouter',
      'vuex': 'Vuex',
      'axios': 'axios',
      'moment': 'moment'
    },
    // Set up all the aliases we use in our app.
    resolve: {
      alias: {
        src: resolveSrc('src')
      }
    },
    plugins: [
      /*
      new webpack.optimize.LimitChunkCountPlugin({
        maxChunks: 6
      })
      */
    ]
  },
  devServer: {
    open: false,
    host: '127.0.0.1',
    port: 8008,
    https: false,
    hotOnly: false,
    proxy: null,
    before: app => {}
  },
  pluginOptions: {
  },
  css: {
    // Enable CSS source maps.
    sourceMap: process.env.NODE_ENV !== 'production'
  }
};
